import React, { Component } from 'react';

import { 
    View,
    Image,
    Text,
} from 'react-native';

import styles from '../styles/common';

class Logo extends Component {
    constructor(props){
        super(props);
    }
    render() {
        return (
            <View style={styles.header}>
                <Image style={styles.image}
                    source={require('../../assets/images/logolbmx.png')}/>
                <Text  style={[styles.light, styles.h2]}>Labmake</Text>
                <Text  style={[styles.bold, styles.h3]}>México</Text>
            </View>)        
    }
}

export default Logo;