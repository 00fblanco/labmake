import React, { Component } from 'react';
import { Image, TouchableOpacity } from 'react-native';
import { Container, Header, 
    Content, Card as BaseCard, 
    CardItem, Thumbnail, 
    Text, Button, 
    Icon, Left, 
    Body, Right
} from 'native-base';
import moment from 'moment';
const Card = ({ publication, onDetail }) => {
    return <TouchableOpacity onPress={() => onDetail()}>
            <BaseCard >
                <CardItem >
                    <Left>
                        <Thumbnail source={{uri: "https://scontent-lax3-2.xx.fbcdn.net/v/t1.0-1/p480x480/16473389_882437718564340_3899079311226126764_n.jpg?_nc_cat=0&_nc_eui2=v1%3AAeEslgqebF_2eCIIhMFa4Qb8rgZvsAquhQlgtBElpk61Nsm0JJkWWqEoAWJJPSf2LDI-VjJwUpdUaX96DlR3PCpNW0P6h_KhNNFgUN1B_duyww&oh=bd4e397920192f3e512e0e358af92124&oe=5B9938D0"}} />
                        <Body>
                            <Text>{publication.title}</Text>
                            <Text note>LabmakeMX</Text>
                        </Body>
                    </Left>
                </CardItem>
                <CardItem>
                    <Left>
                        <Button transparent >
                            <Icon ios="ios-eye" android="md-eye" active  />
                            <Text>{publication.views}</Text>
                        </Button>
                    </Left>
                    <Right>
                        <Text note>{moment(publication.updated_at).utc().fromNow()}</Text>
                    </Right>
                </CardItem>
            </BaseCard>
            </TouchableOpacity>
}

export default Card;
