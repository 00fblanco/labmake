
import React from 'react';
import { View, Text, Button, Icon, H1} from 'native-base';
import { MarkdownView } from 'react-native-markdown-view'

import styles from '../styles/common';

const ModalPublicationDetail = ({navigation}) => {
    const { publication } = navigation.state.params;
    return (
        <View style={{ flex: 1, alignItems: 'center', flexDirection: 'column', justifyContent: 'flex-end'}}>
            <View style={styles.halfModal}>
                <View style={{ flex: 0.1, flexDirection: 'row', justifyContent: 'flex-end', alignSelf: 'stretch', backgroundColor: '#f4f8f9', borderTopLeftRadius: 20,borderTopRightRadius: 20,}}>
                    <Button transparent primary onPress={() => navigation.goBack()}>
                        <Icon style={{ fontSize: 40, color: '#9f1d41' }} ios="ios-close" android="md-close" />
                    </Button>
                </View>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center',alignSelf: 'stretch'}}>
                    <View style={{ flex: 0.1 ,borderBottomColor: '#a0a0a0', borderBottomWidth: 0.5, alignSelf: 'stretch'}}>
                        <H1 style={{alignSelf: 'center'}}>{publication.title}</H1>
                    </View>
                    <View style={{flex: 1, paddingHorizontal: 5}}>
                        <MarkdownView>{publication.content}</MarkdownView>
                    </View>
                </View>
            </View>
        </View>
    );
};

export default ModalPublicationDetail;