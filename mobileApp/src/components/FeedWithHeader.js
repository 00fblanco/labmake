import React, { Component } from 'react';

import { 
    View,
    Image,
    Text,
    Animated,
    ScrollView,
    FlatList,
    Platform
} from 'react-native';
import { Spinner } from 'native-base';

import styles from '../styles/common';
import Card from './Card';

const HEADER_MIN_HEIGHT = 58;
const HEADER_MAX_HEIGHT = 200;

class FeedWithHeader extends Component {
    constructor(props){
        super(props);
        this.scrollYAnimatedValue = new Animated.Value(0);

        this._renderFlatList = this._renderFlatList.bind(this);
    }
    _renderFlatList = () => {
        if(this.props.publications.total){
            return (<FlatList
                keyExtractor={item => item.title}
                refreshing={this.props.refreshing}
                onRefresh={() => this.props.onRefresh()}
                data={this.props.publications.data}
                renderItem={({item}) => <Card key={item.title} onDetail={() => this.props.onDetail(item)} key={item.id} publication={item}/>}

                contentContainerStyle = {{ paddingTop: HEADER_MAX_HEIGHT }}
                scrollEventThrottle = { 16 }
                onScroll = { Animated.event(
                    [{ nativeEvent: { contentOffset: { y: this.scrollYAnimatedValue }}}]
                )}
            ></FlatList>)
        }else{
            return (
                <View style={{flex: 1, justifyContent: 'center', alignContent: 'stretch', alignItems: 'center'}}>
                    <Spinner color='#9f1d41'/>
                </View>
                )
        }
    }
    render() {
        const headerHeight = this.scrollYAnimatedValue.interpolate(
        {
            inputRange: [ 0, ( HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT ) ],
            outputRange: [ HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT ],
            extrapolate: 'clamp'
        });
        
        const headerBackgroundColor = this.scrollYAnimatedValue.interpolate(
        {
            inputRange: [ 0, ( HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT )  ],
            outputRange: [ '#9f1d41', '#e3245b' ],
            extrapolate: 'clamp'
        });
        return (
            <View style={styles.containter}>
                {this._renderFlatList()}
                {this.props.children}
                <Animated.View style = {[ styles.animatedHeader, { height: headerHeight, backgroundColor: headerBackgroundColor } ]}>
                    <View style={styles.header}>
                        <Text  style={[{fontSize: 20}, styles.light, styles.whiteColor, { marginTop : (Platform.OS == 'ios') ? 10 : 0 }]}>Labmake MX</Text>
                        <Text style={[styles.light, styles.h5, styles.whiteColor, {textAlign: 'center', marginTop : (Platform.OS == 'ios') ? 15 : 0}]}>Mantente informado de las publicaciones de labmake MX</Text>
                    </View>
                </Animated.View>
            </View>
        );
        
    }
}

export default FeedWithHeader;