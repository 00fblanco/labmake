import { createStore, compose, applyMiddleware } from 'redux';
import promiseMiddleware from 'redux-promise';
import thunk from 'redux-thunk';

import reducers from '../reducers';

const componseEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const enhancer = componseEnhancers(applyMiddleware(promiseMiddleware, thunk));


export const store = createStore(reducers, {}, enhancer);