import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import userReducer from './userReducer';
import publicationReducer from './publicationReducer';

const reducers = {
    form: formReducer,
    user: userReducer,
    publications: publicationReducer
}

const allReducers= combineReducers(reducers);
export default allReducers;