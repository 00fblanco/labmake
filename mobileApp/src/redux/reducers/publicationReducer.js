export default function publicationReducer(state = {}, action){
    switch(action.type){
        case'LOAD_PUBLICATIONS':
            return action.publications.data
        break;
        default: 
            return state;
    }
}