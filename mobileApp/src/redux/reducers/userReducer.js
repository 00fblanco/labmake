export default function userReducer(state = {}, action){
    switch(action.type){
        case 'LOG_IN':
            console.log('LOG_IN', action.jwt)
            return {...state, jwt: action.jwt}
        case 'LOAD_USER':
            console.log('LOAD_USER', action.user);
            return {...state, user: {
                id: action.user.id,
                email: action.user.email,
                role: action.user.role
            }}
        case 'LOG_OUT':
            return {};
        default:
            return state;
    }
}