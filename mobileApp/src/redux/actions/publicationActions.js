
import { fetchPublications } from '../../../constants/requests';

export function loadPublications(publications){
    return { type: 'LOAD_PUBLICATIONS', publications}
}

export function getPublications(){
    return (dispatch, getState) => {
        fetchPublications()
            .then(publications => {
                dispatch(loadPublications(publications))
            })
    }
}