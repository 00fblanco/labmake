import React, { Component } from 'react';
import { View, Image } from 'react-native';
import { Container, Button, Text, Fab, Icon} from 'native-base';
import { connect } from 'react-redux';
import ButtonComponent from 'react-native-button-component';

import * as publicationActions from '../../redux/actions/publicationActions';

import styles from '../../styles/common';
import colors from '../../../constants/Colors';
import FeedWithHeader from '../../components/FeedWithHeader';

class WelcomeScreen extends Component {
    
    constructor(props){
        super(props);
        this.state = {
            refreshing: false
        }
        this.publicationDetail = this.publicationDetail.bind(this);
        this._refresh = this._refresh.bind(this);
        this.goLogin = this.goLogin.bind(this);

        this.getPublications();
    }
    getPublications(){
        this.props.getPublications();
    }
    publicationDetail(publication){
        this.props.navigation.navigate('ModalPublicationDetail', {
            publication
        });
    }
    _refresh(){
        this.setState({ refreshing: true})
        this.getPublications();
        this.setState({refreshing: false})
    }
    goLogin(){
        this.props.navigation.navigate('Login');
    }
    render() {
        return (
            <Container style={{backgroundColor: colors.$grayColor}}>
                <FeedWithHeader refreshing={this.state.refreshing} onRefresh={this._refresh} onDetail={this.publicationDetail} publications={this.props.publications} >
                    <Fab
                        position="bottomRight"
                        style={{ backgroundColor: colors.$strongRedColor }}
                        onPress={() => this.goLogin()}
                    >
                        <Icon ios="ios-log-in" android="md-log-in"/>
                    </Fab>

                </FeedWithHeader>
            </Container>
         );
        
    }
}

function mapStateToProps(state, ownProps){
    return {
        publications: state.publications
    }
}
export default connect(mapStateToProps, {getPublications: publicationActions.getPublications})(WelcomeScreen);