import React, { Component } from 'react';
import { View, Text, Platform } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import { Container } from 'native-base';

import { Logo, FeedWithHeader } from '../../components'
import commonStyles from '../../styles/common'
import Colors from '../../../constants/Colors';


class FeedScreen extends Component {
    constructor(props){
        super(props);
        this.state = {
            refreshing: false
        }
        this.publicationDetail = this.publicationDetail.bind(this);
        this._refresh = this._refresh.bind(this);
    }
    
    static navigationOptions = {
        headerStyle: {
            backgroundColor: Colors.$redColor
        },
        title: 'Noticias',
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: '400',
            fontSize: 20  
        },
        tabBarIcon: ({ tintColor }) => (
            Platform.select({ ios: <Icon name="ios-paper" size={25} color={tintColor}/>, android: <Icon name="md-paper" size={25} color={tintColor}/> })
        )
    }
    _refresh(){
        this.setState({ refreshing: true})
        this.getPublications();
        this.setState({refreshing: false})
    }
    publicationDetail(publication){
        this.props.navigation.navigate('ModalPublicationDetail', {
            publication
        });
    }
    render() {
        return (
            <Container>
                <FeedWithHeader refreshing={this.state.refreshing} onRefresh={this._refresh} onDetail={this.publicationDetail} publications={this.props.publications} />
            </Container>
        );
    }
}

function mapStateToPros(state, ownProps){
    return {
        publications: state.publications
    }
}
export default connect(mapStateToPros)(FeedScreen);