import React, { Component } from 'react';
import { 
    View,
    Image,
} from 'react-native';
import { Container, Header, Content, Text, Button, Toast } from 'native-base';
import { Field,reduxForm } from 'redux-form';
import { connect } from 'react-redux';

import { Logo, TextField } from '../../components';
import ButtonComponent from 'react-native-button-component/dist/ButtonComponent';
import styles from '../../styles/common';
import colors from '../../../constants/Colors';
import { emailValidation, passwordValidation } from '../../validations';


const validate = values => {
    const error = {}
    
    var email = values.email || '';
    var password = values.password || '';

    error.email = emailValidation(email) || '';
    error.password = passwordValidation(password) || '';
    
    error.curp = {errors: ['']};
    error.fullname = {errors: ['']};
    error.age = {errors: ['']};
    error.job = {errors: ['']};
    error.phone = {errors: ['']};
    error.address = {errors: ['']};
    error.town = {errors: ['']};
    error.state = {errors: ['']};
    return error;
};

class SignupScreen extends Component {
    constructor(props){
        super(props);
        this.handleLogin = this.handleLogin.bind(this);
    }

    handleLogin(){

    }
    render() {
        return (
            <Container style={{backgroundColor: 'white'}}>
                <Logo />
                <Content style={{marginHorizontal: 20, marginBottom: 40}}>
                    <View >
                        <Field label="CURP" name="curp" component={TextField} />
                        <Field label="Nombre y apellidos" name="fullname" component={TextField} />
                        <Field label="Edad" name="age" component={TextField} keyboardType="number-pad" />
                        <Field label="¿A que te dedicas?" name="job" component={TextField} />
                        <Field label="Telefono" name="phone" component={TextField} keyboardType="phone-pad"/>
                        <Field label="Dirección" name="address" component={TextField} />
                        <Field label="Municipio" name="town" component={TextField} />
                        <Field label="Estado" name="state" component={TextField} />

                        <Field label="Correo electrónico" name="email" component={TextField} keyboardType="email-address"/>
                        <Field label="password" name="password" secure={true} component={TextField} />
                    </View>
                    <ButtonComponent
                            style={{ marginTop: 15 }}
                            text="Registrar"
                            type="primary"
                            shape="round"
                            backgroundColors={[colors.$redColor, colors.$strongRedColor]}
                            gradientStart={{ x: 0.4, y: 0.4 }}
                            gradientEnd={{ x: 1, y: 1 }}
                            height={60}
                            onPress={this.handleLogin}
                            />
                </Content>
            </Container>
        );
    }
}

function mapStateToProps (state, ownProps){
    return {
        form: state.form
    }
}

export default reduxForm({
    form: 'signup',
    validate
})(connect(mapStateToProps, {})(SignupScreen))