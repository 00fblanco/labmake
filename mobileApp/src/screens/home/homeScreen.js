import React, { Component } from 'react';
import { View, Text, Button, Platform } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import commonStyles from '../../styles/common'
import Colors from '../../../constants/Colors';


class HomeScreen extends Component {
    static navigationOptions = {
        headerStyle: {
            backgroundColor: Colors.$redColor,
        },
        title: 'Inicio',
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: '400',
            fontSize: 20
        },
        tabBarIcon: ({ tintColor }) => (
            Platform.select({ ios: <Icon name="ios-home" size={25} color={tintColor}/>, android: <Icon name="md-home" size={25} color={tintColor}/> })
            
        )
    }
    render() {
        return (
            <View style={commonStyles.root}>
                <Text>home screen</Text>
            </View>
        );
    }
}

export default HomeScreen;