import React, { Component } from 'react';
import { View, Text, Platform } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import commonStyles from '../../styles/common';
import Colors from '../../../constants/Colors';



class ProfileScreen extends Component {
    static navigationOptions = {
        headerStyle: {
            backgroundColor: Colors.$redColor
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: '400',
            fontSize: 20
        },
        title: 'Perfil',
        tabBarIcon: ({ tintColor }) => (
            Platform.select({ ios: <Icon name="ios-contact" size={25} color={tintColor}/>, android: <Icon name="md-contact" size={25} color={tintColor}/> })
        )
    }
    render() {
        return (
            <View style={commonStyles.root}>
                <Text>Profile screen</Text>
            </View>
        );
    }
}

export default ProfileScreen;