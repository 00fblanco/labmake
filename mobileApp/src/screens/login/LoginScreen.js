import React, { Component } from 'react';
import { 
    View,
    Image,
    Dimensions,
    Keyboard,
} from 'react-native';
import { Container, Header, Content, Text, Button, Toast } from 'native-base';
import { Field,reduxForm } from 'redux-form';
import { connect } from 'react-redux';

import ButtonComponent from 'react-native-button-component';
import colors from '../../../constants/Colors';
import { TextField, Logo } from '../../components';
import { emailValidation, passwordValidation } from '../../validations';
import { login as apiLogin } from '../../../constants/requests';
import * as userActions from '../../redux/actions/userActions';


import styles from '../../styles/common';

const validate = values => {
    const error = {}
    
    var email = values.email || '';
    var password = values.password || '';

    error.email = emailValidation(email) || '';
    error.password = passwordValidation(password) || '';

    return error;
};



class LoginScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showToast: false
        };

        this.handleLogin = this.handleLogin.bind(this);
        this.loadUser = this.loadUser.bind(this);
        this.goSignup = this.goSignup.bind(this);
    }
    handleLogin(){
        const {  login } = this.props.form;
        if(!login.syncErrors.email.approved ||  !login.syncErrors.password.approved){
            return false;
        }
        const { email, password } = login.values;
        const credentials = {
            email, password
        }
        apiLogin(credentials)
            .then(this.loadUser)
            .catch((err) => {
                if(err.response){
                    Toast.show({
                        text: err.response.data.messages,
                        buttonText: "Okay",
                        type: 'warning'
                    })
                }
            })
    }
    loadUser(data){
        const { token, user } = data.data;
        this.props.login(token.token);
        this.props.loadUser(user);
        this.props.navigation.navigate('AppTabNavigator');
    }

    goSignup(){
        this.props.navigation.navigate('Signup');
    }
    render() {
        return (
            <Container style={{backgroundColor: 'white'}}>
                <Logo />
                <Content style={{marginHorizontal: 20}}>
                    <View style={{flex: 1}}>
                        <Field label="Correo electrónico" name="email" component={TextField} keyboardType="email-address" />
                        <Field label="password" name="password" secure={true} component={TextField} />
                        <ButtonComponent
                            style={{ marginTop: 15 }}
                            text="Ingresar"
                            type="primary"
                            shape="round"
                            backgroundColors={[colors.$redColor, colors.$strongRedColor]}
                            gradientStart={{ x: 0.4, y: 0.4 }}
                            gradientEnd={{ x: 1, y: 1 }}
                            height={60}
                            onPress={this.handleLogin}
                            />
                    </View>
                    <View style={styles.footer}>
                        <View style={styles.textRow}>
                            <Text style={[styles.h5, styles.light, styles.marginTop]}>¿No tienes una cuenta?</Text>
                            <Button transparent onPress={() => this.goSignup()}>
                                <Text style={[styles.h5, styles.bold]}>Registrate aquí</Text>
                            </Button>
                        </View>
                    </View>
                </Content>
                </Container>
        );
    }
}

function mapStateToProps (state, ownProps){
    return {
        form: state.form
    }
}
export default 
    reduxForm({
        form: 'login',
        validate
    })(connect(mapStateToProps, {login: userActions.login, loadUser: userActions.loadUser})(LoginScreen))
