import React from 'react';

import { TabNavigator } from 'react-navigation';
import { Text, View } from 'react-native';
import Colors from '../../constants/Colors';

import {
    FeedScreen,
    HomeScreen,
    ProfileScreen
} from '../screens';

export default TabNavigator({
    Inicio: {
        screen: HomeScreen
    },
    Noticias: {
        screen: FeedScreen
    },
    Perfil: {
        screen: ProfileScreen
    },
}, {
swipeEnabled: true,
animationEnabled: true,
tabBarPosition: 'bottom',
tabBarOptions: {
    showLabel: false,
    showIcon: true, // para visualizar el tab en android
    inactiveTintColor: Colors.$grayColor,
    activeTintColor: Colors.$redColor,
    indicatorStyle: {backgroundColor: Colors.$redColor},
    pressColor: Colors.$redColor,
    style: {
        backgroundColor: Colors.$whiteColor
    }
}
});
