import { StackNavigator } from 'react-navigation'

import AppTabNavigator from './AppTabNavigator';

import { 
    LoginScreen,
    WelcomeScreen,
    SignupScreen
} from '../screens';

import { ModalPublicationDetail } from '../components';

// main stack
const MainStack = StackNavigator({
  Welcome: {
    screen: WelcomeScreen,
    navigationOptions:{
      header: null,
    }
  },
  Signup: {
    screen: SignupScreen
  },
  Login: {
    screen: LoginScreen
  },
  AppTabNavigator: {
    screen: AppTabNavigator,
    navigationOptions: {
      headerLeft: null,
    }
  },
}, {
  navigationOptions:{
    gesturesEnabled: false
  }
})

export default StackNavigator({
    Main: {
      screen: MainStack
    },
    ModalPublicationDetail: {
      screen: ModalPublicationDetail,
    }
  },{
      mode: 'modal',
      headerMode: 'none',
      cardStyle:{
        backgroundColor:"transparent",
        opacity: 0.95,
      }
    })
