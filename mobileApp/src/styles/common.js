import EStyleSheet from 'react-native-extended-stylesheet';
import { Platform } from 'react-native';


const styles = EStyleSheet.create({
  halfModal: {
    height: "80%" ,
    width: '100%', 
    backgroundColor:"#fff", 
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 10,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  container:
  {
      flex: 1,
      paddingTop: (Platform.OS == 'ios') ? 20 : 0
  },

  animatedHeader:
  {
      position: 'absolute',
      paddingTop: (Platform.OS == 'ios') ? 20 : 0,
      left: 0,
      right: 0,
      justifyContent: 'center',
      alignItems: 'center',
  },

  headerText:
  {
      color: 'white',
      fontSize: 22
  },

  item:
  {
      backgroundColor: '#E0E0E0',
      margin: 8,
      height: 45,
      justifyContent: 'center',
      alignItems: 'center'
  },
  itemText:
  {
      color: 'black',
      fontSize: 16
  },
  box: {
    backgroundColor: 'white',
    borderRadius: 20,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
  },
  root: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '$whiteColor'
  },
  header: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: .6
  },
  content: {
    justifyContent: 'space-between',
    marginHorizontal: 15,
    flex: 1
  },
  footer: {
    flex: .5
  },
  topContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 400
  },
  bottomContainer: {
    flex: 0.9,
  },
  h1: {
    fontSize: 40
  },
  h2: {
    fontSize: 35
  },
  h3: {
    fontSize: 30
  },
  h4: {
    fontSize: 25
  },
  h5: {
    fontSize: 15
  },
  h6: {
    fontSize: 8
  },
  light: {
    fontFamily: 'Montserrat-Light',
  },
  bold: {
    fontFamily: 'Montserrat-Bold',
  },
  textRow: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  image: {
    height: '50%',
    resizeMode: 'contain'
  },
  
  marginTop: {
    marginTop: 13
  },
  marginBottom: {
    marginBottom: 10
  },
  noMargins:{
    marginVertical: 0,
    paddingVertical: 0
  },
  redColor: {
    color: '#e3245b'
  },
  whiteColor: {
    color: '#fff'
  }
});

export default styles;
