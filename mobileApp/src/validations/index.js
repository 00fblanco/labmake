
var approve = require('approvejs');

export const emailValidation = value => {
  var rules = {
    required: {
      message: '*'
    },
    email: {
      message: 'Correo invalido'
    }
  };
  return approve.value(value , rules);
}

export const passwordValidation = value => {
  var rules = {
    required: {
      message: '*'
    },
    min: {
      min: 6,
      message: 'Minimo 6'
    },
    max: {
      max: 12,
      message: 'Máximo 12'
    }
  }
  return approve.value(value, rules);
}

export const curpValidation = value => {
  var rules = {

  }
  return approve.value(value, rules);
}
  