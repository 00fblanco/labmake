import React, { Component } from 'react';

import { Provider } from 'react-redux';
import EStyleSheet from 'react-native-extended-stylesheet';

import { store } from './src/redux/store/';
import Root from './src/Root';
import Colors from './constants/Colors';

EStyleSheet.build(Colors);
// const instructions = Platform.select({
//   ios: 'Press Cmd+R to reload,\n' +
//     'Cmd+D or shake for dev menu',
//   android: 'Double tap R on your keyboard to reload,\n' +
//     'Shake or press menu button for dev menu',
// });


export default class App extends Component {
  render() {
    return (<Provider store={store}>
              <Root />
            </Provider>);
    }
}
