import axios from 'axios';
const IP = '127.0.0.1';
axios.defaults.baseURL = `http://${IP}:3000/api`;

function login(credentials){
    return axios.post(`/users/auth`, credentials)
}
function fetchPublications() {
    return axios.get('/publications')
}
export { login, fetchPublications }
