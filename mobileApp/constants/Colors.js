export default {
  $blackColor: '#282c34',
  $whiteColor: '#fff',
  $blackBlueColor: '#384259',
  $grayColor: '#f3f3f3',
  $redColor: '#e3245b',
  $strongRedColor: '#9f1d41'
};
