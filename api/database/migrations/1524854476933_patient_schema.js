'use strict'

const Schema = use('Schema')

class PatientSchema extends Schema {
  up () {
    this.create('patients', (table) => {
      table.increments()
      table.string('curp').notNullable().unique()
      table.string('fullname').notNullable().unique()
      table.integer('age').notNullable()
      table.string('job')
      table.string('phone').notNullable()
      table.string('address').notNullable()
      table.string('town').notNullable()
      table.string('state').notNullable()
      table.integer('user_id').unsigned().notNullable().unique().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('patients')
  }
}

module.exports = PatientSchema
