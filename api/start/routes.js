'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

const Route = use('Route')



// users
Route.group(() => {
  Route.post('/', 'UserController.store');
  Route.post('/auth', 'UserController.login');
  Route.delete('/auth', 'UserController.logout')
}).prefix('/api/users');

// publications
Route.group(() => {
  Route.get('/:page?', 'PublicationController.findAll')
  Route.post('/', 'PublicationController.store').middleware(['auth', 'authorization:administrador'])
  Route.get('/:id', 'PublicationController.findById')
}).prefix('/api/publications');

// patients
Route.group(() => {
  Route.post('/', 'PatientController.store')
}).prefix('/api/patients')


