'use strict'

class Authorization {
  async handle ({request, auth, response }, next, schemes) {
    const role = schemes[0];

    if(role === auth.user.role){
      return await next()
    }else{
      response.status(200).json({messages: `No esta autorizado para este recurso, necesita autorización de tipo [${role}]`})
    }
  }
}

module.exports = Authorization
