'use strict'

const Model = use('Model')

class Publication extends Model {

    user(){
        return this.belongsTo('App/Models/User')
    }
  // validate propierties
    static get storeRules(){
        return {
            title: 'required|min:8|max:60|unique:publications,title',
            content: 'required|min:20|max:500',
        }
    }

    static get storeMessages(){
        return {
        'required': '{{field}} es requerido.',
        'min': '{{field}} debe tener como minimo {{argument.0}} caracteres',
        'max': '{{field}} debe tener como maximo {{argument.1}} caracteres',
        'in': 'El rol debe ser {{argument.0}} ó {{argument.1}}'
        }
    }
}

module.exports = Publication
