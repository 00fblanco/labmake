'use strict'

const Model = use('Model')

class User extends Model {
  static boot () {
    super.boot()
    this.addHook('beforeCreate', 'User.hashPassword')
  }

  tokens () {
    return this.hasMany('App/Models/Token')
  }

  publications(){
    return this.hasMany('App/Models/Publication')
  }
  static get hidden(){
    return ['password'];
  }
  // validate propierties
  static get storeRules(){
    return {
      email: 'required|email|unique:users,email',
      password: 'required|min:6|max:20',
      role: 'required|in:paciente,contribuyente'
    }
  }

  static get storeMessages(){
    return {
      'required': '{{field}} es requerido para crear el usuario',
      'email': '{{field}} debe tener formato de un correo ejemplo: 00fblanco@dev.com',
      'min': '{{field}} debe tener como minimo {{argument.0}} caracteres',
      'max': '{{field}} debe tener como maximo {{argument.1}} caracteres',
      'in': 'El rol debe ser {{argument.0}} ó {{argument.1}}'
    }
  }
}

module.exports = User
