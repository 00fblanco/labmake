'use strict'

const Model = use('Model')

class Patient extends Model {
    
    user(){
        return this.belongsTo('App/Models/User')
    }


    // validate propierties
    static get storeRules(){
        return {
            curp: 'required|min:18|max:18|unique:patients,curp',
            fullname: 'required|min:10|max:100|unique:patients,fullname',
            age: 'required|range:1,100',
            job: 'min:3|max:50',
            phone: 'required|integer|min:10|max:15',
            address: 'required|min:10,max:200',
            town: 'required|min:3|max:50',
            state: 'required|min:3|max:50',
        }
    }

    static get errorMessages(){
        return {
        'integer': "{{field}} debe ser numerico entero",
        'unique': "{{field}} debe ser unico, ya existe un registro",
        'range': "{{field}} debe estar entre {{argument.0}} y {{argument.1}}",
        'alpha_numeric': "{{field}} debe ser alfa numérico solamente",
        'required': '{{field}} es requerido para crear el usuario',
        'min': '{{field}} debe tener como minimo {{argument.0}} caracteres',
        'max': '{{field}} debe tener como maximo {{argument.1}} caracteres',
        }
    }
}

module.exports = Patient
