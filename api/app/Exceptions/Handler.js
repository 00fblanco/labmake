'use strict'

const BaseExceptionHandler = use('BaseExceptionHandler')

/**
 * This class handles all exceptions thrown during
 * the HTTP request lifecycle.
 *
 * @class ExceptionHandler
 */
class ExceptionHandler extends BaseExceptionHandler {
  /**
   * Handle exception thrown during the HTTP lifecycle
   *
   * @method handle
   *
   * @param  {Object} error
   * @param  {Object} options.request
   * @param  {Object} options.response
   *
   * @return {void}
   */

  async handle (error, { request, response }) {
    console.log(error.name)
    if(error.name === 'ExpiredJwtToken'){
      return response.status(error.status).json({message: 'JWT ha expirado, vuelva a autenticarse'});
    }else if(error.name === 'ModelNotFoundException'){
      return response.status(error.status).json({ message: 'Recurso no encontrado'})
    }
    
    response.status(error.status).json({ message: error.message})
  }

  /**
   * Report exception for logging or debugging.
   *
   * @method report
   *
   * @param  {Object} error
   * @param  {Object} options.request
   *
   * @return {void}
   */
  async report (error, { request }) {
  }
}

module.exports = ExceptionHandler
