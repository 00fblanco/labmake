'use strict'
const User = use('App/Models/User');
const Patient = use('App/Models/Patient');

const Database = use('Database')
const { validate } = use('Validator');
const Hash = use('Hash');

class PatientController {
  async store ({ request, response }) {
    // user validate
    const userData = request.only(['email', 'password']);
    const validationUser = await validate({...userData, role: 'paciente'}, User.storeRules, User.storeMessages);

    // patient validate
    const patientData = request.only(['curp', 'fullname', 'age', 'job', 'phone', 'address', 'town', 'state']);
    const validationPatient = await validate(patientData, Patient.storeRules, Patient.errorMessages);
    
    if(validationPatient.fails()){
      response.status(401).json({
          messages: validationPatient.messages()
      })
    }else if(validationUser.fails()){
      response.status(401).json({
        messages: validationUser.messages()
      })
    }else{
      const trx = await Database.beginTransaction()
        // Guardamos al usuario
        const user = await User.create({ ...userData }, trx);
        // Guardamos los datos del paciente
        const patient = await Patient.create({...patientData, user_id: user.id }, trx);
        response.status(201).json({ user, patient });
      trx.commit();
    }



  }
}

module.exports = PatientController
