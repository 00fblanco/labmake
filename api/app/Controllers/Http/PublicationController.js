'use strict'
const Publication = use('App/Models/Publication');
const { validate } = use('Validator');

class PublicationController {
    async findAll({ request, response, params }){
        const page = params.page || 1; // default page is 1
        return await Publication.query()
                        .orderBy('id', 'desc')
                        .paginate(page, 20)
    }

    async store({ request, response, auth}){
        const body = request.only(['title', 'content']);
        const validation = await validate(body, Publication.storeRules, Publication.storeMessages);
        if(validation.fails()){
            response.status(401).json({
                messages: validation.messages()
            })
        }else{
            const { title, content } = body;
            const user_id = auth.user.id;
            const publication = await Publication.create({ title, content, user_id })
            response.status(201).json({ publication });
        }
    }

    async findById({ request, response, params }){
        const publication = await Publication.findOrFail(params.id);
        publication.merge({ views: publication.views + 1 });
        await publication.save();
        return publication
    }
}

module.exports = PublicationController
