'use strict'
const User = use('App/Models/User');
const { validate } = use('Validator');
const Hash = use('Hash');


class UserController {

    async logout({ request, response, auth}){
        return await auth
                    .listTokens();
    }
    async login({ request, response, auth }){
        const { email, password } = request.post();
        try{
            const user = await User.findBy('email', email);

            if(!user){
                return response.status(404).json({messages: 'No se encontro el email del usuario.'});
            }
            const verifyPassword = await Hash.verify(password, user.password);
            if(verifyPassword){
                const token = await auth.generate(user);
                return response.status(201).json({ token, user });
            }else{
                return response.status(404).json({messages: 'La contraseña es incorrecta'});
            }
        }catch(error){
            console.log(error);
        }

    }
    async store({ request, response }){
        const body = request.only(['email', 'password', 'role']);
        const validation = await validate(body, User.storeRules, User.storeMessages);

        if(validation.fails()){
            response.status(401).json({
                messages: validation.messages()
            })
        }else{
            const { email, password, role } = body;
            // verificamos si existe un administrador
            const countUsers = await User.query().where({ role: 'administrador'}).count() 
            if(countUsers[0]['count(*)'] > 0){
                // ya existe un administrador
                if(role != 'administrador' && role != undefined){
                    const user = await User.create({ email, password, role });
                    response.status(201).json({ user });
                }else{
                    response.status(401).json({ messages: 'No se pueden crear usuarios si no se indica el rol o con rol de administrador' });
                }
            }else{
                // creamos el usuario como administrador
                const adminRole = 'administrador'
                const user = await User.create({ email, password, role: adminRole });
                response.status(201).json({ user });
            }
        }
        
    }

}

module.exports = UserController
